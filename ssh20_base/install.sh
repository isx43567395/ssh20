#! /bin/bash
# Implantació del servei nss-pam-ldap
rm -rf /etc/openldap/ldap.conf
rm -rf /etc/nsswitch.conf 
rm -rf /etc/nscld.conf
rm -rf /etc/pam.d/system-auth 
cp /opt/docker/ldap.conf /etc/openldap/.
cp /opt/docker/nsswitch.conf /etc/.
cp /opt/docker/nslcd.conf /etc/.
cp /opt/docker/system-auth /etc/pam.d/.
