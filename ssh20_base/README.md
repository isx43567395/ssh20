# SSH

## @Alejandro Lopez ASIX M06 2020-2021


Podeu trobar les imatges docker al Dockehub de [zeuslawl](https://hub.docker.com/u/zeuslawl/)

### Imatges:
zeuslawl/ssh20:base



```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d zeuslawl/ldap20:group

$ docker run --rm --name ssh.edt.org -h ssh.edt.org --net 2hisix -p 2022:22 -it zeuslawl/ssh20:base

detach
$ docker run --rm --name ssh.edt.org -h ssh.edt.org --net 2hisix -p 2022:22 -d zeuslawl/ssh20:base 
